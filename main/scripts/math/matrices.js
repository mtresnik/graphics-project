function transpose(array, m, n){
  if(isSquare(array)){
      m = Math.sqrt(array.length);
      n = m;
  }
  if(array.length != m*n){
    console.error("array length != m*n");
    return null;
  }
  var retArray = [];
  for(var row = 0; row < n; row++){
    for(var col = 0; col < m; col++){
      retArray.push(array[col*n + row]);
    }
  }
  return retArray;
}

function identity(n){
  var retArray = [];
  for(var col = 0; col < n; col++){
    for(var row = 0; row < n; row++){
      if(row == col){
        retArray.push(1);
      }else{
        retArray.push(0);
      }
    }
  }
  return retArray;
}

function det(array){
  if(!isSquare(array)){
    console.error("Determinant must be of a square matrix.");
    return null;
  }
  var n = Math.sqrt(array.length);
  switch(n){
    case 1:
      return array[0];
      break;
    case 2:
      return array[0]*array[3] - array[1]*array[2];
  }
  var sum = 0;
  var currArray = componentMultiply(array, plusMinusArray(n));
  var y = 0;
  // Collumn expansion
  for(var col = 0; col < n; col++){
      sum += currArray[y*n + col]*det(allExcept(array, y, col));
  }
  return sum;
}

function isSquare(array){
  var n = Math.sqrt(array.length);
  return n == Math.floor(n);
}

function allExcept(array, y, x, m, n){
  if(isSquare(array)){
      m = Math.sqrt(array.length);
      n = m;
  }
  var size = m*n;
  var retArray = [];
  for(var row = 0; row < m; row++){
    for(var col = 0; col < n; col++){
      if(!(row == y || col == x)){
        retArray.push(array[row*m + col]);
      }
    }
  }
  return retArray;
}

function componentMultiply(array1, array2){
  if(array1.length != array2.length){
    console.error("array1.length != array2.length");
  }
  var retArray = [];
  for(var i = 0; i < array1.length; i++){
    retArray.push(array1[i]*array2[i]);
  }
  return retArray;
}

function plusMinusArray(length){
  var retArray = [];
  for(var i = 0; i < length*length; i++){
    retArray.push(i % 2 == 0 ? 1 : -1);
  }
  return retArray;
}

function matrixMultiplicationSquare(array1, array2){
  if(!isSquare(array1) || !isSquare(array2)){
    console.error("Inputs must be square matrices.");
    return null;
  }
  var n = Math.sqrt(array1.length);
  return matrixMultiplication(array1, n, n, array2, n, n);
}

function matrixMultiplication(array1, m, n, array2, o, p){
  if(array1.length != m*n){
    console.error("array1.length does not equal m*n");
  }
  if(array2.length != o*p){
    console.error("array2.length does not equal o*p");
  }
  if(n != o){
    console.error("Parameters n and o must be the same.");

  }
  var retArray = [];

  var col = [];
  var row = [];
  for(var i = 0; i < p; i++){
    col = [];
    for(var j = 0; j < o; j++){
      col.push(array2[j*p + i]);
    }
    for(var k = 0; k < p; k++){
    var sum = 0;
      for(var j = 0; j < o; j++){
      row = [];
      sum += (array1[k*o + j] * col[j]);
      }
      retArray.push(sum);
    }
  }
  return transpose(retArray, m, p);
}

function inverse(array){
  if(!isSquare(array)){
    console.error("input matrix isn't square");
    return null;
  }
  var detA = det(array);
  if(detA == 0){
    console.error("cannot take inverse, det = 0");
    return null;
  }
  var n = Math.sqrt(array.length);
  var retArray = identity(n);
  for(var row = 0; row < n; row++){
    for(var col = 0; col < n; col++){
      retArray[row*n + col] = det(allExcept(array, row, col))/detA;
      if((row+col) % 2 != 0){
        retArray[row*n + col]*=-1;
      }
    }
  }
  retArray = transpose(retArray);
  return retArray;
}


/**
 * Translate a mat4 by the given vector
 *
 * @param {mat4} recM the receiving matrix
 * @param {mat4} a the matrix to translate
 * @param {vec3} v vector to translate by
 * @returns {mat4} recM
 */
function translateMat4(recM, a, v) {
  var x = v[0],
      y = v[1],
      z = v[2];
  var a00 = void 0,
      a01 = void 0,
      a02 = void 0,
      a03 = void 0;
  var a10 = void 0,
      a11 = void 0,
      a12 = void 0,
      a13 = void 0;
  var a20 = void 0,
      a21 = void 0,
      a22 = void 0,
      a23 = void 0;

  if (a === recM) {
    recM[12] = a[0] * x + a[4] * y + a[8] * z + a[12];
    recM[13] = a[1] * x + a[5] * y + a[9] * z + a[13];
    recM[14] = a[2] * x + a[6] * y + a[10] * z + a[14];
    recM[15] = a[3] * x + a[7] * y + a[11] * z + a[15];
  } else {
    a00 = a[0];a01 = a[1];a02 = a[2];a03 = a[3];
    a10 = a[4];a11 = a[5];a12 = a[6];a13 = a[7];
    a20 = a[8];a21 = a[9];a22 = a[10];a23 = a[11];

    recM[0] = a00;recM[1] = a01;recM[2] = a02;recM[3] = a03;
    recM[4] = a10;recM[5] = a11;recM[6] = a12;recM[7] = a13;
    recM[8] = a20;recM[9] = a21;recM[10] = a22;recM[11] = a23;

    recM[12] = a00 * x + a10 * y + a20 * z + a[12];
    recM[13] = a01 * x + a11 * y + a21 * z + a[13];
    recM[14] = a02 * x + a12 * y + a22 * z + a[14];
    recM[15] = a03 * x + a13 * y + a23 * z + a[15];
  }

  return recM;
}

/**
 * Scales the mat4 by the dimensions in the given vec3 not using vectorization
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {vec3} v the vec3 to scale the matrix by
 * @returns {mat4} out
 **/
function scaleMat4(out, a, v) {
  var x = v[0],
      y = v[1],
      z = v[2];

  out[0] = a[0] * x;
  out[1] = a[1] * x;
  out[2] = a[2] * x;
  out[3] = a[3] * x;
  out[4] = a[4] * y;
  out[5] = a[5] * y;
  out[6] = a[6] * y;
  out[7] = a[7] * y;
  out[8] = a[8] * z;
  out[9] = a[9] * z;
  out[10] = a[10] * z;
  out[11] = a[11] * z;
  out[12] = a[12];
  out[13] = a[13];
  out[14] = a[14];
  out[15] = a[15];
  return out;
}

/**
 * Rotates a mat4 by the given angle around the given axis
 *
 * @param {mat4} recM the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @param {vec3} axis the axis to rotate around
 * @returns {mat4} recM
 */
function rotateMat4(recM, a, rad, axis) {
  var x = axis[0],
      y = axis[1],
      z = axis[2];
  var len = Math.sqrt(x * x + y * y + z * z);
  var s = void 0,
      c = void 0,
      t = void 0;
  var a00 = void 0,
      a01 = void 0,
      a02 = void 0,
      a03 = void 0;
  var a10 = void 0,
      a11 = void 0,
      a12 = void 0,
      a13 = void 0;
  var a20 = void 0,
      a21 = void 0,
      a22 = void 0,
      a23 = void 0;
  var b00 = void 0,
      b01 = void 0,
      b02 = void 0;
  var b10 = void 0,
      b11 = void 0,
      b12 = void 0;
  var b20 = void 0,
      b21 = void 0,
      b22 = void 0;

  if (Math.abs(len) < glMatrix.EPSILON) {
    return null;
  }

  len = 1 / len;
  x *= len;
  y *= len;
  z *= len;

  s = Math.sin(rad);
  c = Math.cos(rad);
  t = 1 - c;

  a00 = a[0];a01 = a[1];a02 = a[2];a03 = a[3];
  a10 = a[4];a11 = a[5];a12 = a[6];a13 = a[7];
  a20 = a[8];a21 = a[9];a22 = a[10];a23 = a[11];

  // Construct the elements of the rotation matrix
  b00 = x * x * t + c;b01 = y * x * t + z * s;b02 = z * x * t - y * s;
  b10 = x * y * t - z * s;b11 = y * y * t + c;b12 = z * y * t + x * s;
  b20 = x * z * t + y * s;b21 = y * z * t - x * s;b22 = z * z * t + c;

  // Perform rotation-specific matrix multiplication
  recM[0] = a00 * b00 + a10 * b01 + a20 * b02;
  recM[1] = a01 * b00 + a11 * b01 + a21 * b02;
  recM[2] = a02 * b00 + a12 * b01 + a22 * b02;
  recM[3] = a03 * b00 + a13 * b01 + a23 * b02;
  recM[4] = a00 * b10 + a10 * b11 + a20 * b12;
  recM[5] = a01 * b10 + a11 * b11 + a21 * b12;
  recM[6] = a02 * b10 + a12 * b11 + a22 * b12;
  recM[7] = a03 * b10 + a13 * b11 + a23 * b12;
  recM[8] = a00 * b20 + a10 * b21 + a20 * b22;
  recM[9] = a01 * b20 + a11 * b21 + a21 * b22;
  recM[10] = a02 * b20 + a12 * b21 + a22 * b22;
  recM[11] = a03 * b20 + a13 * b21 + a23 * b22;

  if (a !== recM) {
    // If the source and destination differ, copy the unchanged last row
    recM[12] = a[12];
    recM[13] = a[13];
    recM[14] = a[14];
    recM[15] = a[15];
  }
  return recM;
}

/**
 * Generates a perspective projection matrix with the given bounds
 *
 * @param {mat4} recM mat4 frustum matrix will be written into
 * @param {number} fovy Vertical field of view in radians
 * @param {number} aspect Aspect ratio. typically viewport width/height
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} recM
 */
function perspectiveMat4(recM, fovy, aspect, near, far) {
  var f = 1.0 / Math.tan(fovy / 2);
  var nf = 1 / (near - far);

  recM[0] = f / aspect;
  recM[5] = f;
  recM[10] = (far + near) * nf;
  recM[14] = 2 * far * near * nf;

  for (i = 1; i < recM.length; i++) {
    recM[i] = 0;
  }

  return recM;
}

/**
 * Multiplies two quat's
 *
 * @param {quat} recM the receiving quaternion
 * @param {quat} a the first operand
 * @param {quat} b the second operand
 * @returns {quat} recM
 */
function multiplyMat4(recM, a, b) {
  recM[0] = a[0] * b[3] + a[3] * b[0] + a[1] * b[2] - a[2] * b[1];
  recM[1] = a[1] * b[3] + a[3] * b[1] + a[2] * b[0] - a[0] * b[2];
  recM[2] = a[2] * b[3] + a[3] * b[2] + a[0] * b[1] - a[1] * b[0];
  recM[3] = a[3] * b[3] - a[0] * b[0] - a[1] * b[1] - a[2] * b[2];
  return recM;
}

/**
 * Copy the values from one mat4 to another
 *
 * @param {mat4} recM the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} recM
 */
function copyMat4(recM, a) {
  for (i = 0; i < recM.length; i++) {
    recM[i] = a[i];
  }
  return recM;
}

/**
* Calculates a 3x3 normal matrix (transpose inverse) from the 4x4 matrix
*
* @param {mat3} recM mat3 receiving operation result
* @param {mat4} a Mat4 to derive the normal matrix from
*
* @returns {mat3} recM
*/
function normalFromMat4(recM, a) {

  var b00 = a[0] * a[5] - a[1] * a[4];
  var b01 = a[0] * a[6] - a[2] * a[4];
  var b02 = a[0] * a[7] - a[3] * a[4];
  var b03 = a[1] * a[6] - a[2] * a[5];
  var b04 = a[1] * a[7] - a[3] * a[5];
  var b05 = a[2] * a[7] - a[3] * a[6];
  var b06 = a[8] * a[13] - a[9] * a[12];
  var b07 = a[8] * a[14] - a[10] * a[12];
  var b08 = a[8] * a[15] - a[11] * a[12];
  var b09 = a[9] * a[14] - a[10] * a[13];
  var b10 = a[9] * a[15] - a[11] * a[13];
  var b11 = a[10] * a[15] - a[11] * a[14];

  // Calculate the determinant
  var det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

  if (!det) {
    return null;
  }
  det = 1.0 / det;

  recM[0] = (a[5] * b11 - a[6] * b10 + a[7] * b09) * det;
  recM[1] = (a[6] * b08 - a[4] * b11 - a[7] * b07) * det;
  recM[2] = (a[4] * b10 - a[5] * b08 + a[7] * b06) * det;

  recM[3] = (a[2] * b10 - a[1] * b11 - a[3] * b09) * det;
  recM[4] = (a[0] * b11 - a[2] * b08 + a[3] * b07) * det;
  recM[5] = (a[1] * b08 - a[0] * b10 - a[3] * b06) * det;

  recM[6] = (a[13] * b05 - a[14] * b04 + a[15] * b03) * det;
  recM[7] = (a[14] * b02 - a[12] * b05 - a[15] * b01) * det;
  recM[8] = (a[12] * b04 - a[13] * b02 + a[15] * b00) * det;

  return recM;
}

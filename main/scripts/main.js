
var gl = {};
var canvas = null;

function main(){
	/* When the window/document loads this function is called. Here the buttons and their 
	 * respective button functions are set.
     */

     // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

     // 				MIKE HERE ARE THE BUTTON PRESS FUNCTIONS

     // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	window.onload = function() {
		// Assign variables to the cooresponding buttons.
		var lamboBtn = document.getElementById("lamboBtn");
		var challengerBtn = document.getElementById("challengerBtn");
		var pattyBtn = document.getElementById("pattyBtn");
		var uploadBtn = document.getElementById("uploadBtn");

    	// Sets the Lamborghini button call this function when pressed.
    	lamboBtn.onclick = function(event) {
        	lamboBtnPressed(event);
    	}

    	// Sets the Challenger button to call this function when pressed.
    	challengerBtn.onclick = function(event) {
    		challengerBtnPressed(event);
    	}

    	// Sets the Patty Wagon button to call this function when pressed.
    	pattyBtn.onclick = function(event) {
    		pattyBtnPressed(event);
    	}

    	// Sets the upload button to call this function when pressed.
    	uploadBtn.onclick = function(event) {
    		uploadBtnPressed(event);
    	}
 	}

	initGL();
	startModels();
}

function initGL(){
	canvas = document.getElementById("glCanvas");
	gl = gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");

	function testGL(canvas, gl) {
			if (!gl) {
				alert("Unable to initialize WebGL. Your browser or machine may not support it.");
			return;
		} else {
			console.log("GL is present, proceeding to initialize program.");
		}
	}

	testGL(canvas,gl);
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	gl.viewport(0, 0, canvas.width, canvas.height);
}

function lamboBtnPressed(event) {
	console.log("lambo click");
}

function challengerBtnPressed(event) {
	console.log("challenger click");
}

function pattyBtnPressed(event) {
	console.log("patty pressed");
}

function uploadBtnPressed(event) {
	console.log("upload click");
}
function drawCubeDefault(canvas, gl){
	/*
		https://github.com/davidwparker/programmingtil-webgl/blob/master/0061-3d-cube-indices/index.js
		https://www.tutorialspoint.com/webgl/webgl_interactive_cube.htm
	*/


	/* ------------------DEClARE VARIABLES------------------*/

	// ABSTRACT FROM FUNCTION IN OTHER DRAW CUBE METHODS
	var vertices =
		[
			/*v0*/ 0.5, 0.5, 0.5,
			/*v1*/ -0.5, 0.5, 0.5,
			/*v2*/ -0.5, -0.5, 0.5,
			/*v3*/ 0.5, -0.5, 0.5,
			/*v4*/ 0.5, -0.5, -0.5,
			/*v5*/ 0.5,0.5,-0.5,
			/*v6*/ -0.5, 0.5, -0.5,
			/*v7*/ -0.5, -0.5, -0.5
		];
	var indices =
		[
			/*FRONT*/ 	0, 1, 2,	2, 3, 0,
			/*RIGHT*/	0, 3, 4,	4, 5, 0,
			/*TOP*/		0, 5, 6,	6, 1, 0,
			/*LEFT*/	1, 6, 7,	7, 2, 1,
			/*BOTTOM*/	3, 2, 7,	7, 4, 3,
			/*BACK*/	6, 5, 4,	4, 7, 6
		];
    var colors = [
        5,3,7, 5,3,7, 5,3,7, 5,3,7,
        1,1,3, 1,1,3, 1,1,3, 1,1,3,
        0,0,1, 0,0,1, 0,0,1, 0,0,1,
        1,0,0, 1,0,0, 1,0,0, 1,0,0,
        1,1,0, 1,1,0, 1,1,0, 1,1,0,
         0,1,0, 0,1,0, 0,1,0, 0,1,0
        ];


	/* ------------------CREATE BUFFERS------------------*/

	// VERTEX
	     var vertex_buffer = gl.createBuffer ();
         gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
         gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

	// COLORS
	     var color_buffer = gl.createBuffer ();
         gl.bindBuffer(gl.ARRAY_BUFFER, color_buffer);
         gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

	// INDICIES
	     var index_buffer = gl.createBuffer ();
         gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_buffer);
         gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

	/* ------------------SHADERS------------------ */

	// VERTEX
	var vertCode =
		'attribute vec3 position;'+
        'uniform mat4 Proj_Matrix;'+
        'uniform mat4 View_Matrix;'+
        'uniform mat4 Move_Matrix;'+
		// COLOR OF VERTEX
        'attribute vec3 color;'+
		// COLOR OF FACE
        'varying vec3 vColor;'+
        'void main(void) { '+
            'gl_Position = Proj_Matrix*View_Matrix*Move_Matrix*vec4(position, 1.);'+
            'vColor = color;'+
        '}';

	var vertShader = gl.createShader(gl.VERTEX_SHADER);
         gl.shaderSource(vertShader, vertCode);
         gl.compileShader(vertShader);
		var vcompiled = gl.getShaderParameter(vertShader, gl.COMPILE_STATUS);
		if (!vcompiled) {
			console.error(gl.getShaderInfoLog(vertShader));
			console.log("Vertex Shader cannot be compiled.");
		}else{
			console.log("Vertex Shader Compiled!");
		}

	// FRAGMENT
	var fragCode =
		'precision mediump float;'+
        'varying vec3 vColor;'+
        'void main(void) {'+
            'gl_FragColor = vec4(vColor, 1.);'+
        '}';


	var fragShader = gl.createShader(gl.FRAGMENT_SHADER);
        gl.shaderSource(fragShader, fragCode);
        gl.compileShader(fragShader);
		var fcompiled = gl.getShaderParameter(fragShader, gl.COMPILE_STATUS);
		if (!fcompiled) {
			console.error(gl.getShaderInfoLog(fragShader));
			console.log("Fragment Shader cannot be compiled.");
		}else{
			console.log("Fragment Shader Compiled!");
		}

	// PRORGRAM
	var shaderprogram = gl.createProgram();
         gl.attachShader(shaderprogram, vertShader);
         gl.attachShader(shaderprogram, fragShader);
         gl.linkProgram(shaderprogram);
		 var linked = gl.getProgramParameter(shaderprogram, gl.LINK_STATUS);
		 if (!linked) {
			console.error(gl.getProgramInfoLog(shaderprogram));
			console.log("Shader Program not Linked.");
		}else{
			console.log("Shader Program Linked!");
		}

	/* ------------------MATRIX------------------ */
	// GETS POINTER LOCATION FOR UNIFORM VARIABLE IN SHADER PROGRAM
    var _Pmatrix = gl.getUniformLocation(shaderprogram, "Proj_Matrix");
    var _Vmatrix = gl.getUniformLocation(shaderprogram, "View_Matrix");
    var _Mmatrix = gl.getUniformLocation(shaderprogram, "Move_Matrix");

	/*------------------BINDING AND USING PROGRAM------------------*/
	// BIND VERTEX BUFFER
	gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
		// GETS POINTER LOCATION FOR POSITION ATTRIBUTE IN SHADER PROGRAM
        var _position = gl.getAttribLocation(shaderprogram, "position");
		// PASS IN POINTER LOCATION
        gl.vertexAttribPointer(_position, 3, gl.FLOAT, false,0,0);
        gl.enableVertexAttribArray(_position);

	// BIND COLOR BUFFER
    gl.bindBuffer(gl.ARRAY_BUFFER, color_buffer);
		// GETS POINTER LOCATION FOR COLOR ATTRIBUTE IN SHADER PROGRAM
         var _color = gl.getAttribLocation(shaderprogram, "color");
		 // PASS IN POINTER LOCATION
         gl.vertexAttribPointer(_color, 3, gl.FLOAT, false,0,0) ;
         gl.enableVertexAttribArray(_color);

	// USE PROGRAM
	gl.useProgram(shaderprogram);

	/*
	Perspective Matrix:
	http://learnwebgl.brown37.net/08_projections/projections_perspective.html
	*/

	function getProjection(fov, aspect_ratio, near, far){
		// Half the fov
		var angle = Math.tan((fov*0.5)*Math.PI/180);
		// Row 1 Col 1 scales width
		// Row 2 Col 2 scales height
		// Row 3 Col 4 clips object
		return [
			0.5/angle, 0, 0, 0,
			0, 0.5*aspect_ratio/angle, 0, 0,
			0, 0, -(far+near)/(far - near), -1,
			0, 0, -(2*far*near)/(far - near), 0
		];

	}

	var proj_matrix = getProjection(40, canvas.width/canvas.height, 1, 100);
    var mo_matrix = [ 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 ];
    var view_matrix = [ 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 ];

	// Distance away from camera
	view_matrix[14] = view_matrix[14] - 6;


	/* ------------------DRAWING------------------ */
	gl.enable(gl.DEPTH_TEST);

	gl.clearColor(0.5, 0.5, 0.5, 0.9);
    gl.clearDepth(1.0);
    gl.viewport(0.0, 0.0, canvas.width, canvas.height);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.uniformMatrix4fv(_Pmatrix, false, proj_matrix);
    gl.uniformMatrix4fv(_Vmatrix, false, view_matrix);
    gl.uniformMatrix4fv(_Mmatrix, false, mo_matrix);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_buffer);
    gl.drawElements(gl.TRIANGLES, indices.length, gl.UNSIGNED_SHORT, 0);

	console.log("END : drawCubeDefault");
}

// WebGL context
var gl = {};
// the canvas element
var canvas = null;
// main shader program
// main app object
var application = {meshes : {}, models : {}, matrices : {mvMatrix : mat4.create(), vMatrix: mat4.create(), pMatrix : mat4.create(), mvMatrixStack : []}};
var temporal = { timeNow : 0, elapsed : 0, time : 0, lastTime : 0 };
var modelObjects = [];
// Temp global variable
var suzanne = null;
var patty = null;
var square = null;
var doRotate = true;


window.requestAnimFrame = (function() {
    return (
        window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
            return window.setTimeout(callback, 1000 / 60);
        }
    );
})();

function startModels(){
  let p = OBJ.downloadModels([
    {
        obj: "/res/models/suzanne/suzanne.obj",
        mtl: true
    },
    {
      name: "pattywagon",
      obj: "/res/models/patty_wagon/pattywagon.obj",
      mtl: "/res/models/patty_wagon/pattywagon.mtl"
    },
    {
      name: "square",
      obj: "/res/models/test_square/square.obj",
      mtl: "/res/models/test_square/square.mtl"
    }
]);
p.then(models => {
    webGLStart(models);
});
}

function initMeshes(){
  //For each mesh : meshes
  for(var mesh in application.meshes){
    // Set materials
    var materialsByIndex = application.meshes[mesh].materialsByIndex;
    var afterMain = "";
    var beforeMain = "";
    var glFragColor = "";
    var count = 0;
    var images = [];

    // For each material : materialsByIndex
    for(var material in materialsByIndex){
      var mapDiffuse = materialsByIndex[material].mapDiffuse;
      if(!mapDiffuse){
        continue;
      }
      var s_name = "u_image" + count;
      beforeMain += "uniform sampler2D " + s_name + ";\n";
      afterMain += "vec4 color" + count + " =texture2D("+ s_name +", vTextureCoord.st);";
      glFragColor = "gl_FragColor = vec4(color, 1.0) + color"+count +";";
	    images[count] = {js : {texture: mapDiffuse.texture}, gl : {texture:null, index:gl.TEXTURE0 + count}, s : {name : s_name, location: ""}};

      var texture = gl.createTexture();
      gl.bindTexture(gl.TEXTURE_2D, texture);
      gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
      // Set the parameters so we can render any size image.
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

      // Upload the image into the texture.
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, images[count].js.texture);

	    images[count].gl.texture = texture;
      gl.bindTexture(gl.TEXTURE_2D, texture);

      count++;
    }
    console.log(images);


    // Create vertex shader
    application.meshes[mesh].fragmentScript = null;
    var fragmentScriptHtml = document.getElementById("FRAGMENT_SHADER");
    var fragmentScript = "";
    var k = fragmentScriptHtml.firstChild;
    while (k) {
        if (k.nodeType == 3) {
            fragmentScript += k.textContent;
        }
        k = k.nextSibling;
    }
    fragmentScript = fragmentScript.replace("void main(void) {", beforeMain + "\nvoid main(void) {\n" + afterMain);
    fragmentScript = (fragmentScript ? fragmentScript.replace("gl_FragColor = vec4(color, 1.0);", glFragColor) : fragmentScript);
    application.meshes[mesh].fragmentScript = fragmentScript;


    // Set shdaer program
    application.meshes[mesh].shaderProgram = null;
    application.meshes[mesh].shaderProgram = initShaderProgram(fragmentScript, images);
    application.meshes[mesh].images = images;
  }
}

function initBuffers() {
    var layout = new OBJ.Layout(
        OBJ.Layout.POSITION,
        OBJ.Layout.NORMAL,
        OBJ.Layout.DIFFUSE,
        OBJ.Layout.UV,
        OBJ.Layout.SPECULAR,
        OBJ.Layout.SPECULAR_EXPONENT
    );

    // initialize the mesh's buffers
    for (var mesh in application.meshes) {
        // Create the vertex buffer for this mesh
        var vertexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
        var vertexData = application.meshes[mesh].makeBufferData(layout);
        gl.bufferData(gl.ARRAY_BUFFER, vertexData, gl.STATIC_DRAW);
        vertexBuffer.numItems = vertexData.numItems;
        vertexBuffer.layout = layout;
        application.meshes[mesh].vertexBuffer = vertexBuffer;

        // Create the index buffer for this mesh
        var indexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
        var indexData = application.meshes[mesh].makeIndexBufferData();
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indexData, gl.STATIC_DRAW);
        indexBuffer.numItems = indexData.numItems;
        application.meshes[mesh].indexBuffer = indexBuffer;

        // this loops through the mesh names and creates new
        // model objects and setting their mesh to the current mesh
        application.models[mesh] = {};
        var model = application.models[mesh];
        application.models[mesh].mesh = application.meshes[mesh];
        application.models[mesh].mMatrix = mat4.create();
        modelObjects.push(new ModelObject(model.mesh.name, model.mesh, model.mMatrix));
    }
}

function webGLStart(meshes) {
    // Passed in meshes to be concerned about.
    application.meshes = meshes;
    initMeshes();
    initBuffers();
    gl.clearColor(0.5, 0.5, 0.5, 1.0);
    gl.enable(gl.DEPTH_TEST);
    // modelObjects[0].translate([-2, 0, -5]);
    modelObjects[0].translate([0, 0, -5]);
    modelObjects[0].visible = false;
    suzanne = modelObjects[0];


    modelObjects[1].translate([0, -1, -5]);
    modelObjects[1].visible = true;
    patty = modelObjects[1];
    patty.scale(0.75,0.75,0.75);

    square = modelObjects[2];
    square.translate([0,0,-5]);
    square.visible = false;
    tick();
}

class ModelObject{

  constructor(name, mesh, mMatrix){
    this.name = name;
    this.mesh = mesh;
    // Input checking
    this.mMatrix = (!mMatrix ?  mMatrixDefault() : mMatrix);
    this.visible = false;

  }

  /** @param t1 : array (or x-shift)
   *  @param t2 : y-shift
   *  @param t3 : z-shift
   * */
  translate(t1, t2, t3){
    var translator = null;
    if(Array.isArray(t1)){
      translator = t1;
    }else{
      translator = [t1,t2,t3];
    }
    var mMatrix = this.mMatrix;
    mat4.translate(mMatrix, mMatrix, translator);
  }

  /** @param t1 : array (or x-scale)
   *  @param t2 : y-scale
   *  @param t3 : z-scale
   * */
  scale(t1, t2, t3){
    var scalor = null;
    if(Array.isArray(t1)){
      scalor = t1;
    }else{
      scalor = [t1,t2,t3];
    }
    var mMatrix = this.mMatrix;
    mat4.scale(mMatrix, mMatrix, scalor);
  }
  rotate(angle, about){
    var mMatrix = this.mMatrix;
    mat4.rotate(mMatrix, mMatrix, angle, about);
  }
  reset(){
    this.mMatrix = mMatrixDefault();
    this.visible = true;
  }
  setOrigin(new_origin){
    // TODO
  }
  setmMatrix3(new_mMatrix3){
    var toMap = [0,1,2,4,5,6,8,9,10];
    for(var i = 0; i < new_mMatrix3.length; i++){
      this.mMatrix[toMap[i]] = new_mMatrix3[i];
    }
  }


}

function mMatrixDefault(){
  return mat4.translate(mat4.create(), mat4.create(), [0,0,-5]);
}

/** @param vertexShader
    @return dShaderProgram
*/
function initShaderProgram(fragmentScript, images) {
    var fragmentShader = getShader(gl, "FRAGMENT_SHADER");
    var vertexShader = getShader(gl, "VERTEX_SHADER");
  if(fragmentScript){
    fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);;
    gl.shaderSource(fragmentShader, fragmentScript);
    gl.compileShader(fragmentShader);

    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(fragmentShader));
    }
  }
    // var vertexShader = newVertexShader(gl, "VERTEX_SHADER");

    var dShaderProgram = gl.createProgram();
    gl.attachShader(dShaderProgram, vertexShader);
    gl.attachShader(dShaderProgram, fragmentShader);
    gl.linkProgram(dShaderProgram);

    if (!gl.getProgramParameter(dShaderProgram, gl.LINK_STATUS)) {
        alert("Could not initialise shaders");
    }
    gl.useProgram(dShaderProgram);

    const attrs = {
        aVertexPosition: OBJ.Layout.POSITION.key,
        aVertexNormal: OBJ.Layout.NORMAL.key,
        aTextureCoord: OBJ.Layout.UV.key,
        aDiffuse: OBJ.Layout.DIFFUSE.key,
        aSpecular: OBJ.Layout.SPECULAR.key,
        aSpecularExponent: OBJ.Layout.SPECULAR_EXPONENT.key
    };

    dShaderProgram.attrIndices = {};
    for (const attrName in attrs) {
        if (!attrs.hasOwnProperty(attrName)) {
            continue;
        }
        dShaderProgram.attrIndices[attrName] = gl.getAttribLocation(dShaderProgram, attrName);
        if (dShaderProgram.attrIndices[attrName] != -1) {
            gl.enableVertexAttribArray(dShaderProgram.attrIndices[attrName]);
        } else {
            console.warn(
                'Shader attribute "' +
                    attrName +
                    '" not found in shader. Is it undeclared or unused in the shader code?'
            );
        }
    }

    dShaderProgram.pMatrixUniform = gl.getUniformLocation(dShaderProgram, "uPMatrix");
    dShaderProgram.mvMatrixUniform = gl.getUniformLocation(dShaderProgram, "uMVMatrix");
    dShaderProgram.nMatrixUniform = gl.getUniformLocation(dShaderProgram, "uNMatrix");

    dShaderProgram.applyAttributePointers = function(model) {
        const layout = model.mesh.vertexBuffer.layout;
        for (const attrName in attrs) {
            if (!attrs.hasOwnProperty(attrName) || dShaderProgram.attrIndices[attrName] == -1) {
                continue;
            }
            const layoutKey = attrs[attrName];
            if (dShaderProgram.attrIndices[attrName] != -1) {
                const attr = layout[layoutKey];
                gl.vertexAttribPointer(
                    dShaderProgram.attrIndices[attrName],
                    attr.size,
                    gl[attr.type],
                    attr.normalized,
                    attr.stride,
                    attr.offset
                );
            }
        }
    };
    console.log(dShaderProgram);

    for(var ii in images){
      images[ii].s.location = gl.getUniformLocation(dShaderProgram, images[ii].s.name);
    }

    return dShaderProgram;
}

function getShader(gl, id) {
    var shaderScript = document.getElementById(id);
    if (!shaderScript) {
        return null;
    }

    var str = "";
    var k = shaderScript.firstChild;
    while (k) {
        if (k.nodeType == 3) {
            str += k.textContent;
        }
        k = k.nextSibling;
    }

    var shader;
    if (shaderScript.type == "GLSL/fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (shaderScript.type == "GLSL/vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
        return null;
    }

    gl.shaderSource(shader, str);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(shader));
        return null;
    }

    return shader;
}

function tick() {
    // Recursive Function
    requestAnimFrame(tick);
    drawScene();
    animate();
}

function drawScene() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    // pMatrix is for the camera
    mat4.perspective(application.matrices.pMatrix, 45 * Math.PI / 180.0, gl.viewportWidth / gl.viewportHeight, 0.01, 1000.0);

      for(var i = 0; i < modelObjects.length; i++){
        // Need to keep vMatrix as separate object included with camera structure
        var modelObject = modelObjects[i];
        if(!modelObject.visible){
          continue;
        }
        var vMatrix = mat4.create();
        application.matrices.mvMatrix = mat4.multiply(application.matrices.mvMatrix, vMatrix, modelObject.mMatrix);
        if(doRotate){
          mat4.rotate(application.matrices.mvMatrix, application.matrices.mvMatrix, temporal.time * 0.25 * Math.PI, [0, 1, 0]);
        }

        // move the object
    // set up the scene
        mvPushMatrix();
        drawObject(modelObject);
        mvPopMatrix();
      }
}

function mvPushMatrix() {
    var copy = mat4.create();
    mat4.copy(copy, application.matrices.mvMatrix);
    application.matrices.mvMatrixStack.push(copy);
}

function drawObject(modelObject) {
    /*
     Takes in a model that points to a mesh and draws the object on the scene.
     Assumes that the setMatrixUniforms function exists
     as well as the shaderProgram has a uniform attribute called "samplerUniform"
     */
    var shaderProgram = modelObject.mesh.shaderProgram;
    gl.useProgram(shaderProgram);

    gl.bindBuffer(gl.ARRAY_BUFFER, modelObject.mesh.vertexBuffer);
    shaderProgram.applyAttributePointers(modelObject);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, modelObject.mesh.indexBuffer);
    setMatrixUniforms(shaderProgram);
    for(var ii in modelObject.mesh.images){
      var image = modelObject.mesh.images[ii];
      // Tell WebGL we want to affect texture unit 0
      gl.activeTexture(image.gl.index);

      // Bind the texture to texture unit 0
      gl.bindTexture(gl.TEXTURE_2D, image.gl.texture);

      // Tell the shader we bound the texture to texture unit 0
      gl.uniform1i(image.s.location, ii);

      // gl.bindTexture(gl.TEXTURE_2D, null);
    }
    gl.drawElements(gl.TRIANGLES, modelObject.mesh.indexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    // gl.drawArrays(gl.TRIANGLE_STRIP, 0, modelObject.mesh.vertexBuffer.numItems);
}

function setMatrixUniforms(shaderProgram) {
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, application.matrices.pMatrix);
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, application.matrices.mvMatrix);

    var normalMatrix = mat3.create();
    mat3.normalFromMat4(normalMatrix, application.matrices.mvMatrix);
    gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, normalMatrix);
}

function mvPopMatrix() {
    if (application.matrices.mvMatrixStack.length === 0) {
        throw "Invalid popMatrix!";
    }
    application.matrices.mvMatrix = application.matrices.mvMatrixStack.pop();
}

// Update temporal components
function animate() {
    temporal.timeNow = new Date().getTime();
    temporal.elapsed = temporal.timeNow - temporal.lastTime;
    if (!temporal.time) {
        temporal.time = 0.0;
    }
    temporal.time += temporal.elapsed / 1000.0;
    if (temporal.lastTime !== 0) {
    }
    temporal.lastTime = temporal.timeNow;
}


function loadModel(fileLocation, loadMtl){
  let p = OBJ.downloadModels(
    [
      {
          obj: fileLocation,
          mtl: loadMtl
      }
  ]);

  if(p){
    for ([name, mesh] of Object.entries(models)) {
        console.log("Name:", name);
        console.log("Mesh:", mesh);
    }
    return p;
  }else{
    console.log("fuc");
  }
}

function loadStrings(fileLocation){
	var string = "";

	try{
		string = readFile(fileLocation);
	}catch(e){
		// It works
	}

	var lines = string.split("\n");
	return lines;
}

function readFile(file){
    var rawFile = new XMLHttpRequest();
    var opened = rawFile.open("GET", file, false);
	var allText = "";
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
							allText = rawFile.responseText;
            }
        }
    }
    rawFile.send(null);
	return allText.replace(/(?:\\[rn]|[\rn]+)+/g, "");
}
